die Versionsnummer von Java: 11

# Architecture Ref. Card 03

Spring Boot Application mit RDS Mariadb Database, mit CI/CD automatisch auf ECS publizieren

## Inbetriebnahme auf AWS

**Voraussetzungen**

- AWS account mit Zugriff auf RDS, ECR und ECS
- IDE
- Gitlab runner

**Anleitung**

1. Repository Forken 

2. AWS ECR Repository erstellen

3. Unter Settings/CI/CD/Variables folgende Variablen erstellen:
	
- AWS_ACCESS_KEY_ID (Bei lernerlab unter AWS Details zu finden)

- AWS_DEFAULT_REGION (Bei lernerlab unter AWS Details zu finden)

- AWS_SECRET_ACCESS_KEY (Bei lernerlab unter AWS Details zu finden)

- AWS_SESSION_TOKEN (Bei lernerlab unter AWS Details zu finden)

- CI_AWS_ECR_REGISTRY (Erster Teil der Repository URI)

- CI_AWS_ECR_REPOSITORY_NAME (Zweiter Teil der Repository URI)

4. AWS RDS Mariadb Datenbank erstellen (Public machen und nötige Ports freischalten)

5. Projekt auf AWS ECR puschen (zwei möglichkeiten)

- Manuell pushen

- Mit Gitlab CI/CD pushen (Deploy stage wird im moment noch nicht funktionieren)

6. AWS ECS Task-Definition erstellen 

- URI der Docker image nehmen

- Nötige Ports freischalten

- Folgende Umgebungsvariablen erstellen:

    - DB_URL (Endpoint der RDS Datenbank)

    - DB_USERNAME (Username der RDS Datenbank)

    - DB_PASSWORD (Passwort der RDS Datenbank)

7. AWS ECS Cluster erstellen

8. AWS Service erstellen

- Nötige Ports freischalten

- Task definition verwenden

9. Folgende Variablen unter Settings/CI/CD/Variables erstellen

- CI_AWS_ECS_CLUSTER (Name der ECS Cluster)

- CI_AWS_ECS_SERVICE (Name der ECS Service)
	
- CI_AWS_ECS_TASK_DEFINITION (Name der Task definition)

**Nun wird bei jedem push der Service upgedated**

**Bei Lernerlab muss bei jedem Durchgang die Gitlab Variablen verändert werden!**